var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var connect = require('gulp-connect');
var changed = require('gulp-changed');
var autoprefixer = require('gulp-autoprefixer');
var uncss = require('gulp-uncss');
var gulpif = require('gulp-if');
var sprite = require('css-sprite').stream;

var copy = function(src,dest) {
    return gulp.src(src,  { base: 'src' })
    	.pipe(changed(dest))
        .pipe(gulp.dest(dest));
};

var paths = {
	js: ['src/js/vendor/*.js', 'src/js/app.js'],
	less: ['src/less/main.less', 'src/less/vendor.less'],
	html: ['src/*.html'],
	watch: {
		js: ['src/js/**/*.js'],
		less: ['src/less/**/*.less'],
		html: ['src/*.html']
	},
	copy : ['src/*.html', 'src/img/*.*', 'src/fonts/*.*']
}

gulp.task('less', ['sprites'], function () {
	return gulp.src(paths.less)
	.pipe(less({compress: false}))
	.pipe(autoprefixer({
		cascade: false
	}))
	/* After everything, don't forget to add classes added by JS to the ignore part */
	 .pipe(uncss({
         html: ['src/*.html'],
         ignore: ['header .icons .search.active', 'header .icons .search.active input', '.collapse.in', 'header .jumbo .image-1.active', 'header .jumbo .image-2.active']
     }))
	.on('error', function(err){ console.log(err.message); })
	.pipe(gulp.dest('dist/css'))
	.pipe(connect.reload());
});
gulp.task('sprites', function () {
	return gulp.src('src/img/sprites/*.*')
	.pipe(sprite({
		name: 'sprite',
		style: 'sprite.less',
		cssPath: 'src/css',
		processor: 'less',
		template: 'src/less/components/sprites.mustache'
	}))
	.pipe(gulpif('*.png', gulp.dest('src/img'), gulp.dest('src/less/components')))
});
gulp.task('scripts', function () {
	return gulp.src(paths.js)
	.pipe(concat('all.js'))
	//.pipe(uglify())
	.pipe(gulp.dest('dist/js'))
	.pipe(connect.reload());
});
gulp.task('html', function() {
	return gulp.src(paths.html)
	.pipe(connect.reload());
});
gulp.task('copy', function () {
	return copy(paths.copy, 'dist/');
});
gulp.task('connect', ['less', 'scripts', 'copy'], function() {
  return connect.server(
  	{
  		root: 'dist',
  		livereload: true
  	});
});
gulp.task('default', ['sprites', 'less', 'scripts', 'copy', 'connect'], function () {
	gulp.watch(paths.watch.less, ['less']);
	gulp.watch(paths.watch.js, ['scripts']);
	gulp.watch(paths.watch.html, ['copy', 'html']);
});