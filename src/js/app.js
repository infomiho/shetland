(function () {
var app = {};
app.search = function () {
	var expanded_width = 216,
		collapsed_width = 30;
	$(".search .icon").click(function (e) {
		if($(this).closest(".search").hasClass("active") || $(this).closest(".search").hasClass("always-open")) {
			//SEARCH FUNCTION
			alert("search");
		} else if (!$(this).closest(".search").hasClass("always-open")) {
			var search = $(this).closest(".search");
			search.animate({ width: expanded_width }, "linear", function () {
				search.addClass("active");
				search.find("input").focus();
			});
		}
		e.preventDefault();
	});
	$(".search input").click(function (e) { e.stopPropagation(); e.preventDefault(); });
	$(document).click(function (e) {
		if(!$(e.target).is('.search.active .icon') || !$(e.target).is('.search.always-open .icon')) {
			if($('.search').hasClass('active')) {
				var search = $('.search.active');
				search.removeClass('active');
				search.animate({ width: collapsed_width }, "linear");
			}
		}
	});
};
app.throttle = function (callback, limit) {
    var wait = false;                 // Initially, we're not waiting
    return function () {              // We return a throttled function
        if (!wait) {                  // If we're not waiting
            callback.call();          // Execute users function
            wait = true;              // Prevent future invocations
            setTimeout(function () {  // After a period of time
                wait = false;         // And allow future invocations
            }, limit);
        }
    }
}
app.scroll = function () {
	var navbarheight = $(".navbar").outerHeight();
	smoothScroll.init({
		offset: navbarheight,
		speed: 700
	}
	);
};
app.scrollSpy = function () {
	var scrollSpyItems = [],
		navbarheight = $(".navbar").outerHeight();
	$(".stalker a").each(function (i, item) {
		var href = $(item).attr("href");
		if ($(href).length) {
			scrollSpyItems.push(href);
		}
	});
	//console.log(scrollSpyItems);
	$(window).scroll(function () {
		var windowTop = $(window).scrollTop(),
			candidate = 0,
			candidate_top = 0;
		for (var i = 0; i < scrollSpyItems.length; i++) {
			var itemTop = $(scrollSpyItems[i]).offset().top - navbarheight - 5;
			if (windowTop >= itemTop && itemTop > candidate_top) {
				candidate = i;
				candidate_top = itemTop;
			}
		}
		$(".stalker .icon.big").removeClass("big").addClass("small");
		$(".stalker a[href="+scrollSpyItems[candidate]+"]").removeClass("small").addClass("big");
	});
};
/*app.stalker = function () {
	$(".stalker .icon").click(function () {
		if ($(this).hasClass("small")) {
			$(".stalker").find(".icon.big").removeClass("big").addClass("small");
			$(this).removeClass("small").addClass("big");
		}
	});
};*/
app.vhPoly = function () {
	var jumbo = $(".jumbo .container");
	var inner = $(".jumbo .inner");
	var height = jumbo.outerHeight();
	var margin = 80;
	height = height < inner.outerHeight() + margin ? inner.outerHeight() + margin : height;
	jumbo.height(height);
	var throttled = app.throttle(function () {
		var height = $(this).outerHeight();
		height = height < inner.outerHeight() + margin ? inner.outerHeight() + margin : height;
		jumbo.height(height);
	}, 150);
	$(window).resize(throttled);
};
app.menu = function () {
	var hidden = true;
	var menu = $(".menu");
	var particle = menu.find(".particle");
	$(".nav li a").hover(function () {
		var offset = $(this).offset().left + $(this).outerWidth() / 2 - 15;
		particle.css('left', offset);
		var submenu = $(this).data("submenu");
		menu.find('.submenu').hide();
		menu.find('.submenu-' + submenu).show();
		//if we've gone to the next item, the menu needs to stay visible
		hidden = false;
		menu.show();
	});
	$(".menu").mouseleave(function () {
		//dekstop breakpoint
		hidden = true;
		setTimeout(function () {
			//if the mouse really left - to prevent flicker
			if ($(window).width() > 1200 && hidden) menu.hide();
		}, 100);
	});
	$(".navbar-toggle").click(function () {
		$(".submenu").hide();
		$(".menu").find(".submenu-mobile").addClass("hidden");
		$(".menu").find(".submenu-mobile.root").removeClass("hidden");
		$(".menu").toggle();
	});
	$(".submenu-toggle").click(function () {
		var menu = $(".menu");
		var submenu = $(this).data("submenu");
		menu.find(".submenu").hide();
		menu.find(".submenu-mobile").addClass("hidden");
		menu.find('.submenu-mobile.' + submenu).removeClass("hidden");
	});
	var throttled = app.throttle(function () {
		$(".menu").hide();
	}, 200);
	$(window).resize(throttled);
};
app.running;
app.sliderDots = function () {
	var dots = $(".slider-dots .dot"),
		current = 0;

	var process = function (that) {
		if (that.hasClass("big")) return;

		var id = that.data("id"),
			slide = $(".slide.slide-" + id),
			parent = that.closest(".slider-dots"),
			target1 = "image-1";
			target2 = "image-2";

		current = parseInt(id) - 1;
		parent.find(".dot.big").removeClass("big").addClass("small");
		that.removeClass("small").addClass("big");
		$(".slide.active").removeClass("active");
		slide.addClass("active");
		$("." + target2).attr("style", "background-image: url(" + slide.data("image") + ")");
		$("." + target1).addClass("active");
		setTimeout(function () {
			$("." + target1).removeClass(target1).removeClass("active").addClass(target2 + "_");
			$("." + target2).removeClass(target2).addClass(target1);
			$("." + target2 + "_").removeClass(target2 + "_").addClass(target2);
		}, 500);
	};
	var animate = function () {
		if (!$(dots[current]).length) {
			current = 0;
		}
		if (!$(dots[current]).length) return;
		process($(dots[current]));
		app.running = setTimeout(function () {
			current++;
			animate();
		}, 5000);
	};
	animate();
	$(".slider-dots .dot").click(function (e) {
		process($(this));
		clearTimeout(app.running);
		app.running = setTimeout(function () {
			current++
			animate();
		}, 5000);
		e.preventDefault();
	});
	$(".slide").each(function (i, item) {
		var image = $(item).data("image");
		load = new Image();
		load.src = image;
	});
}
app.init = function () {
	var page = $("body").attr("id");
	switch(page) {
		case "walking":
			app.scrollSpy();
			app.vhPoly();
			break;
		case "index":
			app.vhPoly();
			app.sliderDots();
			break;
		default:
			//not needed
	}
	app.search();
	app.scroll();
	app.menu();
};

$(document).ready(app.init);

})();